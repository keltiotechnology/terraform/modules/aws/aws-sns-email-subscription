resource "aws_sns_topic" "default" {
  name = var.sns_topic_name
  tags = var.tags
}

resource "aws_sns_topic_policy" "default" {
  arn    = aws_sns_topic.default.arn
  policy = data.aws_iam_policy_document.sns_topic_policy.json
}

data "aws_iam_policy_document" "sns_topic_policy" {
  statement {
    effect  = "Allow"
    actions = ["SNS:Publish"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }

    resources = [aws_sns_topic.default.arn]
  }
}

resource "aws_sns_topic_subscription" "user_notify" {
  for_each  = toset(var.subscribers)
  topic_arn = aws_sns_topic.default.arn
  protocol  = var.protocol
  endpoint  = each.key
}
