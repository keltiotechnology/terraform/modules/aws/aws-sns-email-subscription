# SNS Topic Subscription

## Usage

```hcl-terraform
module "sns_subscription_state_change" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-sns-email-subscription"
  sns_topic_name = "<SNS TOPIC NAME>"
  subscribers    = ["user@example.com"]
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_sns_topic.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic) | resource |
| [aws_sns_topic_policy.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_policy) | resource |
| [aws_sns_topic_subscription.user_notify](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_subscription) | resource |
| [aws_iam_policy_document.sns_topic_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_protocol"></a> [protocol](#input\_protocol) | Protocol to use. Valid values are: email, sms | `string` | `"email"` | no |
| <a name="input_sns_topic_name"></a> [sns\_topic\_name](#input\_sns\_topic\_name) | Name of SNS topic | `string` | n/a | yes |
| <a name="input_subscribers"></a> [subscribers](#input\_subscribers) | List of user email addresses or sms of users who subscribes to the SNS topic | `list(string)` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tag of the SNS topic resource | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_sns_topic_arn"></a> [aws\_sns\_topic\_arn](#output\_aws\_sns\_topic\_arn) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
