variable "sns_topic_name" {
  type        = string
  description = "Name of SNS topic"
}

variable "protocol" {
  type        = string
  description = "Protocol to use. Valid values are: email, sms"
  default     = "email"
}

variable "subscribers" {
  type        = list(string)
  description = "List of user email addresses or sms of users who subscribes to the SNS topic"
}

variable "tags" {
  type        = map(string)
  description = "Tag of the SNS topic resource"
  default     = {}
}
